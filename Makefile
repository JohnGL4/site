all: push build deploy

push:
	git push

build:
	JEKYLL_ENV=production jekyll build

deploy:
	rsync -rvzP _site/ root@partidopirata.com.ar:/srv/http/ewwlo.void.partidopirata.com.ar
