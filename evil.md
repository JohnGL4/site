---
layout: default
permalink: /evil
---

# /e/vil

eelo is a "foundation" that got over 200k euros in funding from [kickstarter](https://www.kickstarter.com/projects/290746744/eelo-a-mobile-os-and-web-services-in-the-public-in/posts/2108532) and [indiegogo](https://www.indiegogo.com/projects/eelo-a-mobile-os-and-web-services-with-values#/).
they promise to create a mobile OS and web services that are open and secure, and maintains your privacy.

what are the problems with eelo, you may ask? well, a lot of them.

## th/e/y don't do anything new

their "mobile OS" is confirmed to be based on LineageOS 14.1 (a custom ROM based on Android 7.1 (that's old!)) with microG and other open source apps with it.
this already exists. it's called [Lineage for microG](https://lineage.microg.org/), and it has been out for a long time.

## th/e/y still haven't removed a lot of google junk

the ROM [still falls back to google's DNS](https://gitlab.e.foundation/e/os/android_frameworks_base/blob/807180fc2cc49c33663fe6a4adb867760731393b/core/res/res/values/config.xml#L1654)
(this means google knows all the websites you visit when there isn't any other DNS available)
<!--, [still uses google's location servers](https://gitlab.e.foundation/e/os/android_frameworks_base/blob/807180fc2cc49c33663fe6a4adb867760731393b/core/res/res/values/config.xml#L2498) (this means your location is sent to google!!)-->.

## th/e/y use pre-built applications (possibly malware)

instead of building all apps from the source code (the proper way, to assure that there isn't malware), 31 apps that come with the ROM are pre-built which is dangerous as you don't know if those apps contain malware in them.

![proof](images/screenshot_prebuilt_proof.png)
[link](https://gitlab.e.foundation/e/os/android_prebuilts_prebuiltapks/tree/92a28212b5249a2493e6a08731f90a9a29c00b04)

some apps, like Focus, are even clearly downloaded from APKPure, which is a site that re-uploads apps from Play Store(!).

![apkpure proof](images/screenshot_prebuilt_apks.jpg)
[link](https://gitlab.e.foundation/e/os/android_prebuilts_prebuiltapks/tree/92a28212b5249a2493e6a08731f90a9a29c00b04/Focus)

## th/e/y steal code

at one point, they were literally just removing the Lineage copyright header from their code and adding theirs. we told them to stop, so they did something that's basically [the same thing](https://web.archive.org/web/20190520170248/https://gitlab.e.foundation/e/os/android_packages_apps_Jelly/commit/e56dfae8bafd30f5ff0be8965cfdae1d3281ce6a) ("others" is not credit):

![a screenshot showing the commit where they steal code](images/screenshot_steal_code.jpg)

## th/e/y don't accept negative feedback

it is known (tm) that in their telegram chat they love to delete negative feedback and censure their users' opinions.

![a screenshot showing a conversation with someone from the eelo team saying dumb stuff](images/screenshot_patrick_conversation.png)
![a screenshot showing a conversation with someone from the eelo group saying dumb stuff](images/screenshot_you_have_to_believe.jpg)

i hope you now understand why eelo is shit. if you want to contact me (add something funny, bash me for insulting your precious wasted $200k) email me at `v0id at riseup dot net`.

join the ~~love~~ hate club: telegram group [@ewwlo](https://telegram.me/ewwlo)
