---
layout: default
---

# /e/ and Wikipedia

/e/ has been discovered to [have abused Wikipedia](https://en.wikipedia.org/wiki/Special:Contributions/Caliwing) [[2]](https://en.wikipedia.org/wiki/Wikipedia:Sockpuppet_investigations/Mnair69) via multiple [sock-puppet](https://en.wikipedia.org/wiki/WP:SOCK) users to promote themselves and their /e/ dis-product.

It began when the [article on /e/](https://en.wikipedia.org/wiki//e/_(operating_system)) was nominated for deletion, which saw multiple single purpose accounts created to falsely oppose the AFD (Article For Deletion) nomination, and resulted in an [initial sock-puppet investigation](https://en.wikipedia.org/wiki/Wikipedia:Sockpuppet_investigations/Mnair69/Archive) being launched, unfortunately without effect.

Later, the sock-puppet accounts began adding false, unnecessary and promotional information to the /e/ article (see history starting from [here](https://en.wikipedia.org/w/index.php?title=/e/_(operating_system)&offset=20190906201103&action=history)).

After that, a "controversy and criticism" section was added to the article by Wikipedian [Yae4](https://en.wikipedia.org/wiki/User:Yae4) which was immediately targeted by sock-puppet users [Caliwing](https://en.wikipedia.org/wiki/User:Caliwing) and [Indidia](https://en.wikipedia.org/wiki/User:Indidia) [2](https://en.wikipedia.org/wiki/Special:Contributions/Indidea).

Even though the section was later removed for being considered biased by the greater Wikipedia community, a [new sock-puppet investigation](https://en.wikipedia.org/wiki/Wikipedia:Sockpuppet_investigations/Mnair69) was launched, and resulted in the two sock-puppet users being blocked from further Wikipedia activity.

Unfortunately, [Mnair69](https://gitlab.e.foundation/manojnair) [[2]](https://en.wikipedia.org/wiki/User:Mnair69), who has a clear conflict of interest with the /e/ article working for the /e/ foundation, has not yet been exposed and blocked.

We at ewwlo will continue working to make that so.

Gaël has also used [Wikipedia sock-puppetry](https://en.wikipedia.org/w/index.php?title=Ga%C3%ABl_Duval&action=history) (specifically, edits by the aforementioned sock-puppet accounts) to promote himself and his Mandrake GNU/Linux dis-distribution.

Ewwlo has a chart of all known /e/ sock-puppet and single-purpose accounts: 

| Account name                                                      | Blocked | Created on         | Primary activity                                                                  |
|-------------------------------------------------------------------|---------|--------------------|-----------------------------------------------------------------------------------|
| [Caliwing](https://en.wikipedia.org/wiki/User:Caliwing)           | Yes     | 10th February 2017 | Promoting /e/, Gael, and Mandrake GNU/Linux                                       |
| [Indidea](https://en.wikipedia.org/wiki/User:Indidea)             | Yes     | 14th June 2013     | See Caliwing                                                                      |
| [~~Patrick lp~~](https://en.wikipedia.org/wiki/User:Patrick_lp)   | No      | 7th December 2018  | Opposing the deletion nomination for the /e/ article                              |
| [Amitkma](https://en.wikipedia.org/wiki/User:Amitkma)             | No      | 3rd December 2018  | See Patrick lp                                                                    |
| [Olivierd13](https://en.wikipedia.org/wiki/User:Olivierd13)       | No      | 15th May 2013      | See Patrick lp                                                                    |
| [FranckLefevre](https://en.wikipedia.org/wiki/User:FranckLefevre) | No      | 29th November 2011 | See Patrick lp, and small amounts of insignificant editing to add legitimacy      |
| [AlexduCens](https://en.wikipedia.org/wiki/User:AlexduCens)       | No      | 3rd December 2018  | See Patrick lp, and generic vandalism later reverted by the Wikipedia edit filter |

**NOTE**: Patrick LP may be a real person (https://t.me/ewwlo/7406)

Check here for more updates.

This article is Copyright (c) 2019 would (oldosfan), and may be used freely under the terms of either the GNU Free Documentation License,
[version 1.3](https://www.gnu.org/licenses/fdl-1.3.en.html)/any later version published by the Free Software Foundation, or the terms of
the [Peer Production License](https://wiki.p2pfoundation.net/Peer_Production_License).
